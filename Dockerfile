FROM openjdk:11
ARG MONGO_URI
EXPOSE 9000
ARG JAR_FILE=target/ninjaBank-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar", "-DMONGO_URI=$MONGO_URI"]