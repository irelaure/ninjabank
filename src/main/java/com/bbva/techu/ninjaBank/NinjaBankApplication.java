package com.bbva.techu.ninjaBank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NinjaBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(NinjaBankApplication.class, args);
	}
}
