package com.bbva.techu.ninjaBank.controllers;


import com.bbva.techu.ninjaBank.exceptions.AccountAlreadyExistException;
import com.bbva.techu.ninjaBank.exceptions.AccountNotFoundException;
import com.bbva.techu.ninjaBank.models.AccountModel;
import com.bbva.techu.ninjaBank.services.AccountService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ninjaBank/v1/accounts")

public class AccountController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;

    @ApiOperation(
            value="Busca una cuenta por id cuenta",
            notes="Devuelve los datos completos de un document AccountModel"
    )
    @ApiResponse(code = 200, message = "Consulta ejecutada con exito y devuelve un AccountModel")

    @GetMapping("/{id}")
    public ResponseEntity<AccountModel> getAccountsById (
            @ApiParam(defaultValue="No aplica" ,value="Valor de cuenta valido en formato Long")
            @PathVariable Long id)

        throws AccountNotFoundException {
        return new ResponseEntity<>(this.accountService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(
            value="Busca una cuenta por id cliente",
            notes="Devuelve los datos completos de la cuenta Account Model de ese cliente"
    )
    @ApiResponse(code = 200, message = "Consulta ejecutada con exito y devuelve un AccountModel")

    @GetMapping("/client/{id}")
    public ResponseEntity<List<AccountModel>> getAccountsByIdClient (
            @ApiParam(defaultValue="No aplica" ,value="Valor de cliente valido > 0 en formato Long")
            @PathVariable Long id)
            throws AccountNotFoundException {
        return new ResponseEntity<>(this.accountService.findByClient(id), HttpStatus.OK);
    }

    @ApiOperation(
            value="Busca todas las cuentas",
            notes="Devuelve los datos completos de las cuentas AccountModel de la base Mongo"
    )
    @ApiResponse(code = 200, message = "Consulta ejecutada con exito y devuelve un AccountModel")

    @GetMapping()
    public ResponseEntity<List<AccountModel>> getAccounts ()
            throws AccountNotFoundException {
        return new ResponseEntity<>(this.accountService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(
            value="Crea una nueva cuenta con metodo POST",
            notes="Devuelve la nueva cuenta creada en funcion de JSON de peticion"
    )
    @ApiResponse(code = 200, message = "Creacion de nueva cuenta con exito y devuelve un AccountModel insertado")

    @PostMapping
    public ResponseEntity<AccountModel> newAccount(@RequestBody AccountModel new_account)
            throws AccountAlreadyExistException {
        return new ResponseEntity<>(this.accountService.addAccount(new_account), HttpStatus.CREATED);
    }

    @ApiOperation(
            value="Actualiza todos los campos de las cuentas con metodo PUT",
            notes="Devuelve los datos completos actualiados en funcion de JSON de peticion"
    )
    @ApiResponse(code = 200, message = "Consulta ejecutada con exito y devuelve un AccountModel actualizado")

    @PutMapping
    public ResponseEntity<AccountModel>  updateAccount(@RequestBody AccountModel account)
            throws AccountNotFoundException {
        return new ResponseEntity<>(this.accountService.updateAccount(account), HttpStatus.OK);
    }

    @ApiOperation(
            value="Actualiza parcialmente los campos de las cuentas con metodo PATCH",
            notes="Devuelve los datos completos actualiados en funcion de JSON de peticion"
    )
    @ApiResponse(code = 200, message = "Consulta ejecutada con exito y devuelve un AccountModel actualizado")
    @PatchMapping
    public ResponseEntity<AccountModel> patchAccount(@RequestBody AccountModel prod) throws AccountNotFoundException {
        return new ResponseEntity<>(this.accountService.patchAccount(prod), HttpStatus.OK);
    }

    @ApiOperation(
            value="Borra 1 cuenta seleccionada por su id Cuenta",
            notes="No devuelve codigo de retorno , salvo que no exista la cuenta a borrar"
    )
    @ApiResponse(code = 200, message = "Consulta ejecutada ( void) sino se encuenta AccountNotFoundException ")

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAccount(@PathVariable Long id) throws AccountNotFoundException {
        this.accountService.deleteAccount(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(
            value="Actualizacion de saldo en funcion de la cuenta",
            notes="Devuelve el saldo actualizado de la consulta"
    )
    @ApiResponse(code = 200, message = "Consulta realizada con exito")

    @PostMapping("/{accountId}/addMoney")
    public ResponseEntity<AccountModel> addMoney(@PathVariable Long accountId, @RequestBody Double money) throws AccountNotFoundException {
        return new ResponseEntity<>(this.accountService.addMoney(accountId, money),HttpStatus.OK);
    }
}
