package com.bbva.techu.ninjaBank.controllers;

import com.bbva.techu.ninjaBank.exceptions.BadRequestException;
import com.bbva.techu.ninjaBank.exceptions.ClientAlreadyExistsException;
import com.bbva.techu.ninjaBank.exceptions.ClientNotFoundException;
import com.bbva.techu.ninjaBank.models.ClientModel;
import com.bbva.techu.ninjaBank.services.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Validated
@RequestMapping("/ninjaBank/v1/clients")
public class ClientController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @GetMapping
    public ResponseEntity<List<ClientModel>> getClients(@RequestParam(name="$orderBy", required = false) String orderBy) {
        return new ResponseEntity<>(this.clientService.findAll(orderBy), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<ClientModel> getClient(@PathVariable Long id) throws ClientNotFoundException {
        return new ResponseEntity<>(this.clientService.findById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ClientModel> addClient(@RequestBody ClientModel clientModel) throws ClientAlreadyExistsException, BadRequestException {
        return new ResponseEntity<>(this.clientService.createClient(clientModel), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ClientModel> updateClient(@PathVariable Long id, @RequestBody ClientModel client)
            throws ClientNotFoundException {
        return new ResponseEntity<>(this.clientService.updateClient(id, client), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<ClientModel> patchClient(@PathVariable Long id,
                                               @RequestBody ClientModel prod) throws ClientNotFoundException {
        return new ResponseEntity<>(this.clientService.patchClient(id, prod), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteClient(@PathVariable Long id) throws ClientNotFoundException {
        this.clientService.deleteClient(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
