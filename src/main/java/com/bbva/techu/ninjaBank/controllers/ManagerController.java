package com.bbva.techu.ninjaBank.controllers;

import com.bbva.techu.ninjaBank.exceptions.ManagerNotFoundException;
import com.bbva.techu.ninjaBank.models.ClientModel;
import com.bbva.techu.ninjaBank.models.ManagerModel;
import com.bbva.techu.ninjaBank.services.ManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ninjaBank/v1/managers")

public class ManagerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManagerController.class);

    @Autowired
    ManagerService managerService;

    @GetMapping
    public ResponseEntity<List<ManagerModel>> getManagers (@RequestParam(name = "$orderBy", required = false) String orderBy) {
        LOGGER.info("getManagers en ManagerController");
        return new ResponseEntity<>(this.managerService.findAll(orderBy), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ManagerModel> getManagerById (@PathVariable Long id) throws ManagerNotFoundException {
        LOGGER.info("getManagerById en ManagerController");
        return new ResponseEntity<>(this.managerService.findById(id), HttpStatus.OK);
    }

    @GetMapping("/{id}/clients")
    public ResponseEntity<List<ClientModel>> getClientsFromManager (@PathVariable Long id) throws ManagerNotFoundException {
        LOGGER.info("getClientsFromManager en ManagerController");
        return new ResponseEntity<>(this.managerService.clientsFromManager(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ManagerModel> addManager (@RequestBody ManagerModel manager)  {
        LOGGER.info("addManager. ID: "+manager.getId());
        return new ResponseEntity<>((this.managerService.createManager(manager)), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateManager (@RequestBody ManagerModel manager, @PathVariable Long id) throws ManagerNotFoundException {
        LOGGER.info("updateManager. ID: "+manager.getId());

        //NOTA: Este check quizas sobre si en el body ya no enviamos el ID
        if (manager.getId().equals(id)) {
            this.managerService.updateManager(manager, id);
            return new ResponseEntity<>("UPDATED", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("No coinciden el ID_URL vs ID_BODY", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteManager (@PathVariable Long id) throws ManagerNotFoundException {
        LOGGER.info("deleteManager. ID: "+id);
        this.managerService.deleteManager(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/{id}")
    public ResponseEntity<String>  addClient (@PathVariable Long id, @RequestParam(name = "client", required = true) Long clientId)
            throws ManagerNotFoundException {
        LOGGER.info("Manager ID: "+id+". addClient. ClientID: "+clientId);
        this.managerService.addClient(id, clientId);
        return new ResponseEntity<>("liente añadido al gestor", HttpStatus.CREATED);
    }

    @DeleteMapping("/client/{id}")
    public ResponseEntity<String> removeClient (@PathVariable Long id, @RequestParam(name = "client", required = true) Long clientId)
            throws ManagerNotFoundException {
        this.managerService.removeClient(id, clientId);
        return new ResponseEntity<>("Cliente borrado del gestor", HttpStatus.OK);
    }
}
