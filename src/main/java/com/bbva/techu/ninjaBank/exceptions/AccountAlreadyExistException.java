package com.bbva.techu.ninjaBank.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class AccountAlreadyExistException extends RuntimeException {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountAlreadyExistException.class);
    public AccountAlreadyExistException(String id) {
        super("Product with id " + id + " already exists");
        LOGGER.error("Product with id " + id + " already exists");
    }
}

