package com.bbva.techu.ninjaBank.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class AccountNotFoundException extends RuntimeException {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountNotFoundException.class);
    public AccountNotFoundException(Long id) {
        super ("Account with idAccount " + id + " not found");
        LOGGER.error("Account with idAccount " + id + " not found");
    }
}



