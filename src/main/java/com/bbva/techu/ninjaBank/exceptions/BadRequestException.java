package com.bbva.techu.ninjaBank.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class BadRequestException extends Exception{
    private static final Logger LOGGER = LoggerFactory.getLogger(BadRequestException.class);
    public BadRequestException (String message) {
        super ();
        LOGGER.error(message);
    }
}
