package com.bbva.techu.ninjaBank.exceptions;

import com.bbva.techu.ninjaBank.services.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class ClientAlreadyExistsException extends RuntimeException {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientAlreadyExistsException.class);
    public ClientAlreadyExistsException (String dni) {
        super ("Client with dni " + dni + " already exists");
        LOGGER.error("Client with dni " + dni + " already exists");
    }
}