package com.bbva.techu.ninjaBank.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class ClientNotFoundException extends RuntimeException {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientNotFoundException.class);
    public ClientNotFoundException (Long id) {
        super ("Client with id " + id + " not found");
        LOGGER.error("Client with id " + id + " not found");
    }
}
