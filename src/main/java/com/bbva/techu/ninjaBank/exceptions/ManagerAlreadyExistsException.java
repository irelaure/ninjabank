package com.bbva.techu.ninjaBank.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class ManagerAlreadyExistsException extends RuntimeException {
    private static final Logger LOGGER = LoggerFactory.getLogger(ManagerAlreadyExistsException.class);
    public ManagerAlreadyExistsException(Long id) {
        super ("Manager with id " + id + " already exists");
        LOGGER.error("Manager with id " + id + " already exists");
    }
}