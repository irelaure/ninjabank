package com.bbva.techu.ninjaBank.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "accounts")
public class AccountModel {

    @Transient
    public static final String SEQUENCE_NAME = "accounts_sequence";

    @Id
    private Long id;

    @NonNull
    private String accountNumber;
    @NonNull
    private Long idClient;
    @NonNull
    private double accountBalance;
    @NonNull
    private String currency;

}
