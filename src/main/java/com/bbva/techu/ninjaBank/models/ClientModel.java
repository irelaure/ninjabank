package com.bbva.techu.ninjaBank.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "clients")
public class ClientModel {

    @Transient
    public static final String SEQUENCE_NAME = "clients_sequence";
    @Id
    private Long id;

    @NonNull
    private String name;
    @NonNull
    private String surname;
    @NonNull
    private String dni;

    @JsonFormat(pattern="dd-MM-yyyy")
    private Date birthDate;
}
