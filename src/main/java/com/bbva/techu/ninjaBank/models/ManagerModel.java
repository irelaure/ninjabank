package com.bbva.techu.ninjaBank.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "managers")
public class ManagerModel {

    @Transient
    public static final String SEQUENCE_NAME = "managers_sequence";
    @Id
    private Long id;
    private String name;
    private int age;
    //Un Gestor tiene una lista de clientes
    private ArrayList<Long> clientsId;

}

