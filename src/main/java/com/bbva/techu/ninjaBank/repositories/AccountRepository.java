package com.bbva.techu.ninjaBank.repositories;

import com.bbva.techu.ninjaBank.models.AccountModel;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Document(collection = "accounts")
public interface AccountRepository extends MongoRepository <AccountModel, Long> {
    List<AccountModel> findByIdClient(Long idClient);
}
