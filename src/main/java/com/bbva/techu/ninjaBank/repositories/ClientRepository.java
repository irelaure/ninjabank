package com.bbva.techu.ninjaBank.repositories;

import com.bbva.techu.ninjaBank.models.ClientModel;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Document(collection = "clients")
public interface ClientRepository extends MongoRepository<ClientModel, Long> {
    Optional<ClientModel> findFirstByDni(String dni);
}
