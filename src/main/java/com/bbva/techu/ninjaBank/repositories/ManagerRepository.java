package com.bbva.techu.ninjaBank.repositories;

import com.bbva.techu.ninjaBank.models.ManagerModel;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
@Document(collection = "managers")
public interface ManagerRepository extends MongoRepository<ManagerModel, Long> {

}

