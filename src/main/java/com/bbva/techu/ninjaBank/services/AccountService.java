package com.bbva.techu.ninjaBank.services;

import com.bbva.techu.ninjaBank.exceptions.AccountAlreadyExistException;
import com.bbva.techu.ninjaBank.exceptions.AccountNotFoundException;
import com.bbva.techu.ninjaBank.models.AccountModel;
import com.bbva.techu.ninjaBank.repositories.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service

public class AccountService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientService.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;

    public AccountModel findById (Long id) throws AccountNotFoundException {
        LOGGER.debug("Account Service by ID");
        return this.accountRepository.findById(id).orElseThrow(() -> new AccountNotFoundException(id));
    }

    public List<AccountModel> findByClient (Long idClient) throws AccountNotFoundException {
        LOGGER.debug("Account Service by ID");
        return this.accountRepository.findByIdClient(idClient);
    }

    public List<AccountModel> findAll () {
        LOGGER.debug("Account Service by ID");
        return this.accountRepository.findAll();
    }

    public AccountModel addAccount(AccountModel new_account) throws AccountAlreadyExistException {
        LOGGER.debug("New Account Service");

        new_account.setId(sequenceGeneratorService.generateSequence(new_account.SEQUENCE_NAME));

        if (this.accountRepository.findById(new_account.getId()).isPresent()) {
            throw new AccountAlreadyExistException(new_account.getAccountNumber());
        }
        return this.accountRepository.insert(new_account);
    }

    public AccountModel updateAccount(AccountModel account_upd )  throws AccountNotFoundException
    {
        LOGGER.debug("Update Account Service");
        return this.accountRepository.findById(account_upd.getId()).orElseThrow(() -> new AccountNotFoundException(account_upd.getId()));
    }

    public AccountModel patchAccount (AccountModel acc_request )  throws AccountNotFoundException {
        LOGGER.debug("Patch Account Service");

        Optional<AccountModel> acc_resultado = this.accountRepository.findById(acc_request.getId());

        if (acc_resultado.isPresent() ) {
            if (!Objects.isNull(acc_request.getIdClient()))
                acc_resultado.get().setIdClient(acc_request.getIdClient());

            if (!Objects.isNull(acc_request.getCurrency()))
                acc_resultado.get().setCurrency(acc_request.getCurrency());

            if (acc_request.getAccountBalance() != 0.0F )
                acc_resultado.get().setAccountBalance(acc_request.getAccountBalance());

        } else {
            throw new AccountNotFoundException(acc_request.getId());
        }

        return this.accountRepository.save(acc_resultado.get());
    }

    public AccountModel addMoney(Long accountId, double money)  throws AccountNotFoundException {
        AccountModel account = this.accountRepository.findById(accountId).orElseThrow(() -> new AccountNotFoundException(accountId));
        account.setAccountBalance(account.getAccountBalance() + money);
        return this.accountRepository.save(account);
    }

    public void deleteAccount (Long id) throws AccountNotFoundException {
        Optional<AccountModel> acc_delete= this.accountRepository.findById(id);

        if (acc_delete.isPresent())
            this.accountRepository.deleteById(id);
        else
            throw new AccountNotFoundException(id);
    }
}

