package com.bbva.techu.ninjaBank.services;


import com.bbva.techu.ninjaBank.exceptions.BadRequestException;
import com.bbva.techu.ninjaBank.exceptions.ClientAlreadyExistsException;
import com.bbva.techu.ninjaBank.exceptions.ClientNotFoundException;
import com.bbva.techu.ninjaBank.models.ClientModel;
import com.bbva.techu.ninjaBank.repositories.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.isNull;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientService.class);

    public List<ClientModel> findAll(String orderBy) {
        LOGGER.debug("Se van a recuperar todos los clientes");
        return !isNull(orderBy) ? this.clientRepository.findAll(Sort.by(Sort.Direction.ASC, orderBy)) :
                this.clientRepository.findAll();
    }

    public ClientModel findById(Long id) throws ClientNotFoundException {
        LOGGER.debug("Se va a recuperar el cliente con el id " + id);
        return this.clientRepository.findById(id).orElseThrow(() -> new ClientNotFoundException(id));
    }

    public ClientModel createClient(ClientModel client) throws ClientAlreadyExistsException, BadRequestException {
        LOGGER.debug("Se va a dar de alta un cliente");

        if (isNull(client.getDni()) || client.getDni().isEmpty()) {
            throw new BadRequestException("El campo dni es obligatorio");
        }

        client.setId(sequenceGeneratorService.generateSequence(client.SEQUENCE_NAME));
        this.clientRepository
                .findFirstByDni(client.getDni())
                .ifPresent(s -> {
                            throw new ClientAlreadyExistsException(client.getDni());
                        }
                );
        return this.clientRepository.save(client);
    }

    public ClientModel updateClient(Long id, ClientModel newClient) throws ClientNotFoundException {
        LOGGER.debug("Se va a actualizar el cliente con id " + id);
        this.findById(id);
        return this.clientRepository.save(newClient);
    }

    public ClientModel patchClient(Long id, ClientModel newClient) throws ClientNotFoundException {
        LOGGER.debug("Se va a actualizar el cliente con id " + id);
        ClientModel client = this.findById(id);
        if (!isNull(newClient.getName())) {
            client.setName(newClient.getName());
        }
        if (!isNull(newClient.getSurname())) {
            client.setSurname(newClient.getSurname());
        }
        if (!isNull(newClient.getBirthDate())) {
            client.setBirthDate(newClient.getBirthDate());
        }
        if (!isNull(newClient.getDni())) {
            client.setDni(newClient.getDni());
        }
        return this.clientRepository.save(client);
    }

    public void deleteClient(Long id) throws ClientNotFoundException {
        LOGGER.debug("Se va a borrar el cliente con id " + id);
        this.findById(id);
        clientRepository.deleteById(id);
    }
}
