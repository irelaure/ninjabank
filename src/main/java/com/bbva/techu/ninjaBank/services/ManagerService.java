package com.bbva.techu.ninjaBank.services;

import com.bbva.techu.ninjaBank.exceptions.ClientAlreadyExistsException;
import com.bbva.techu.ninjaBank.exceptions.ClientNotFoundException;
import com.bbva.techu.ninjaBank.exceptions.ManagerAlreadyExistsException;
import com.bbva.techu.ninjaBank.exceptions.ManagerNotFoundException;
import com.bbva.techu.ninjaBank.models.ClientModel;
import com.bbva.techu.ninjaBank.models.ManagerModel;
import com.bbva.techu.ninjaBank.repositories.ManagerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;

@Service
public class ManagerService {

    @Autowired
    private ManagerRepository managerRepository;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    private ClientService clientService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ManagerService.class);

    public List<ManagerModel> findAll(String orderBy) {
        LOGGER.debug("findAll: Devolviendo Managers");
        return !isNull(orderBy) ? this.managerRepository.findAll(Sort.by(Sort.Direction.ASC, orderBy)) :
                this.managerRepository.findAll();
    }

    public ManagerModel findById(Long id) throws ManagerNotFoundException {
        LOGGER.debug("findById: Devolver Manager por ID");
        return this.managerRepository.findById(id).orElseThrow(() -> new ManagerNotFoundException(id));
    }

    public ManagerModel createManager (ManagerModel manager)  {
        LOGGER.debug("createManager: Nuevo Manager");

        manager.setId(sequenceGeneratorService.generateSequence(manager.SEQUENCE_NAME));
        manager.setClientsId(new ArrayList<>());
        this.managerRepository
                .findById(manager.getId())
                .ifPresent(s -> {
                            throw new ManagerAlreadyExistsException(manager.getId());
                        }
                );
        return this.managerRepository.save(manager);
    }

    public void updateManager (ManagerModel manager, Long id) throws ManagerNotFoundException {
        LOGGER.debug("updateManager: Actualizando Manager ID: "+id);
        this.findById(id);
        this.managerRepository.save(manager);
    }

    public void deleteManager(Long id) throws ManagerNotFoundException {
        LOGGER.debug("deleteManager: Borrando Manager");
        this.findById(id);
        managerRepository.deleteById(id);
    }

    public List<ClientModel> clientsFromManager (Long idManager) throws ManagerNotFoundException {
        LOGGER.debug("clientsFromManager: Obtener Clientes del Manager");
        ManagerModel manager = this.findById(idManager);

        //Obtener los IDs de CLientes
        List<ClientModel> clients = new ArrayList<>();
        if (!manager.getClientsId().isEmpty())
        {
            for (Long idClient: manager.getClientsId()) {
                clients.add(clientService.findById(idClient));
            }
        }
        return clients;
    }

    public void addClient (Long idManager, Long clientId) throws ClientAlreadyExistsException, ClientNotFoundException, ManagerNotFoundException {
        LOGGER.debug("addClient: Añadir cliente al Manager");
        //0.Check if the Manager exists
        ManagerModel manager = this.findById(idManager);

        //1. Si existe el manager vemos si existe el cliente
        ClientModel client = clientService.findById(clientId);

        //2. Comprobamos si el cliente aun NO esta en el listado del Manager, si ESTA lanza excepcion
        ArrayList<Long> clientsList = manager.getClientsId();

        if (clientsList.contains(clientId)) {
            throw new ClientAlreadyExistsException(client.getDni());
        }
        else{
            clientsList.add(clientId);
            manager.setClientsId(clientsList);
            this.updateManager(manager, manager.getId());
        }
    }

    public void removeClient (Long idManager, Long clientId) throws ClientAlreadyExistsException, ClientNotFoundException, ManagerNotFoundException {
        //0.Check if the Manager exists
        ManagerModel manager = this.findById(idManager);

        //1. Si existe el manager vemos si existe el cliente
        ClientModel client = this.clientService.findById(clientId);

        //2. Comprobamos si el cliente lo tiene asignado el manager
        ArrayList<Long> clientsList = manager.getClientsId();

        manager.getClientsId().removeIf(e -> e.equals(clientId));
    }

}
