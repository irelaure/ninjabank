package com.bbva.techu.ninjaBank;

import com.bbva.techu.ninjaBank.models.AccountModel;
import com.bbva.techu.ninjaBank.repositories.AccountRepository;
import com.bbva.techu.ninjaBank.services.AccountService;
import com.bbva.techu.ninjaBank.services.SequenceGeneratorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private SequenceGeneratorService sequenceGeneratorService;

    @InjectMocks
    private AccountService accountService;

    @Test
    public void shouldReturnAllAccounts() {
        List<AccountModel> accounts = new ArrayList<>();
        accounts.add(new AccountModel());

        given(accountRepository.findAll()).willReturn(accounts);

        List<AccountModel> expected = this.accountService.findAll();

        assertEquals(expected, accounts);

        verify(accountRepository).findAll();
    }

    @Test
    public void whenSaveAccount_shouldReturnAccount() {
        AccountModel account = new AccountModel();
        account.setAccountNumber("1234 5678 9012");

        when(accountRepository.insert(ArgumentMatchers.any(AccountModel.class))).thenReturn(account);

        AccountModel created = this.accountService.addAccount(account);

        assertEquals(created.getAccountNumber(),account.getAccountNumber());
        verify(accountRepository).insert(account);
    }

    @Test
    public void removeAccountById_whenDeleteMethod() throws Exception {
        AccountModel account = new AccountModel();
        account.setAccountNumber("1234 5678 9012");
        account.setId(1L);

        Optional<AccountModel> optAccount = Optional.of(account);

        when(accountRepository.findById(1L)).thenReturn(optAccount);

        this.accountService.deleteAccount(account.getId());

        // then
        verify(accountRepository, times(1)).deleteById(account.getId());
    }
}
