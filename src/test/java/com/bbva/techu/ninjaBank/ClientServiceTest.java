package com.bbva.techu.ninjaBank;

import com.bbva.techu.ninjaBank.models.ClientModel;
import com.bbva.techu.ninjaBank.repositories.ClientRepository;
import com.bbva.techu.ninjaBank.services.ClientService;
import com.bbva.techu.ninjaBank.services.SequenceGeneratorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private SequenceGeneratorService sequenceGeneratorService;

    @InjectMocks
    private ClientService clientService;

    @Test
    public void shouldReturnAllClients(){
        List<ClientModel> clients = new ArrayList<>();

        clients.add(new ClientModel());
        given(clientRepository.findAll()).willReturn(clients);

        List<ClientModel> expected = this.clientService.findAll(null);

        assertEquals(expected, clients);

        verify(clientRepository).findAll();
    }

}

