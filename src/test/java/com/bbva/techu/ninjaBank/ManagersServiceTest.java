package com.bbva.techu.ninjaBank;

import com.bbva.techu.ninjaBank.models.AccountModel;
import com.bbva.techu.ninjaBank.models.ManagerModel;
import com.bbva.techu.ninjaBank.repositories.ManagerRepository;
import com.bbva.techu.ninjaBank.services.ManagerService;
import com.bbva.techu.ninjaBank.services.SequenceGeneratorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ManagersServiceTest {

    @Mock
    private ManagerRepository managerRepository;

    @Mock
    private SequenceGeneratorService sequenceGeneratorService;

    @InjectMocks
    private ManagerService managerService;

    @Test
    public void shouldReturnAllManagers() {
        List<ManagerModel> managers = new ArrayList<>();
        managers.add(new ManagerModel());
        given(managerRepository.findAll()).willReturn(managers);

        List<ManagerModel> expected = this.managerService.findAll(null);

        assertEquals(expected, managers);

        verify(managerRepository).findAll();
    }

    @Test
    public void whenSaveManager_shouldReturnManager() {
        ManagerModel manager = new ManagerModel();
        manager.setName("Pepito");
        manager.setAge(64);

        when(managerRepository.save(ArgumentMatchers.any(ManagerModel.class))).thenReturn(manager);
        ManagerModel expected = this.managerService.createManager(manager);

        assertEquals(expected,manager);
        verify(managerRepository).save(manager);
    }

}
